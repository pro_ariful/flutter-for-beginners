import 'package:flutter/material.dart';

void main() {
  // WidgetApp //MaterialApp //CupertinoApp
  runApp(MaterialApp(
      home: HomePage(),
      theme: ThemeData(
        primarySwatch: Colors.purple,
      )));
}

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Mobile Application")),
      body: Center(
        child: Text("Hello Flutter"),
      ),
    );
  }
}
